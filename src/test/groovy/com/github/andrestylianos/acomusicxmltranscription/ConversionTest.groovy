package com.github.andrestylianos.acomusicxmltranscription

import com.github.andrestylianos.acomusicxmltranscription.util.Conversion
import spock.lang.Specification

class ConversionTest extends Specification {

//    @Unroll('O sustenido de #x deve ser #y')
    def "deve retornar o sustenido da nota"() {
        expect:
        Conversion.converteParaSustenido(x) == y

        where:
        x << ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]
        y << ["A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A"]
    }

//    @Unroll('O duplo sustenido de #x deve ser #y')
    def "deve retornar o duplo sustenido da nota"() {
        expect:
        Conversion.converteParaDuploSustenido(x) == y

        where:
        x << ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]
        y << ["B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#"]
    }

//    @Unroll('O bemol de #x deve ser #y')
    def "deve retornar o bemol da nota"() {
        expect:
        Conversion.converteParaBemol(x) == y

        where:
        x << ["A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A"]
        y << ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]
    }

//    @Unroll('O duplo bemol de #x deve ser #y')
    def "deve retornar o duplo bemol da nota"() {
        expect:
        Conversion.converteParaDuploBemol(x) == y

        where:
        x << ["A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A"]
        y << ["G#", "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G"]
    }
}
