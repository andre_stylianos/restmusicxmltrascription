package com.github.andrestylianos.acomusicxmltranscription

import com.audiveris.proxymusic.ScorePartwise
import com.audiveris.proxymusic.util.Marshalling
import com.github.andrestylianos.acomusicxmltranscription.util.XMLParser
import groovy.io.FileType
import spock.lang.Specification

class XMLParserSpec extends Specification {

    def getAllMusicXML() {
        def folder = System.getProperty("user.dir")+"/src/test/resources"
        def xmls = []
        new File(folder).eachFileRecurse(FileType.FILES) {
            if (it.name.endsWith('.xml')) {
                xmls << it.absolutePath
            }
        }
        return xmls
    }

    def "Parser novo deve obter mesmo resultado do antigo"() {
        setup:
        Marshalling.getContext(ScorePartwise.class)


        when:

        ScorePartwise sp = (ScorePartwise) Marshalling.unmarshal(new FileInputStream(new File(path.toString())));

        def xomresult = XMLParser.parseXML(sp)
        def jaxbresult = XMLParser.parseXML(sp)

        then:
        xomresult == xomresult
        notThrown(Exception)

        where:
        path << getAllMusicXML()

    }
}
