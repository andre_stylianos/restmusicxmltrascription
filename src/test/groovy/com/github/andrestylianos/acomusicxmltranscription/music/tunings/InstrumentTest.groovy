package com.github.andrestylianos.acomusicxmltranscription.music.tunings

import spock.lang.Specification

class InstrumentTest extends Specification {
    def "GetInstrument"() {
        when:
        def instrument = "violao"
        def tuning = "eadgbe"
        def chosentuning = Instrument.getInstrument(instrument).getTuning(tuning)

        then:
        chosentuning != null

    }
}
