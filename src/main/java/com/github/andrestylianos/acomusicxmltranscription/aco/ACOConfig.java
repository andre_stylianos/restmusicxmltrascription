package com.github.andrestylianos.acomusicxmltranscription.aco;

public interface ACOConfig {

    int getNumberOfIterations();

    double getA();

    double getB();

    void incrementB();

    double getQ();

    double getP();

    int getMaxStagnation();

    double getInitialTrail();
}
