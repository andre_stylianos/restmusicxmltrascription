package com.github.andrestylianos.acomusicxmltranscription.aco;

import com.github.andrestylianos.acomusicxmltranscription.exceptions.InvalidMoveException;
import com.github.andrestylianos.acomusicxmltranscription.util.Conversion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.ReentrantLock;

public class Ant implements Callable<List<Position>> {

    private static ReentrantLock traillock = new ReentrantLock();
    List<Position> solution = new ArrayList<>();
    Position startingposition;
    Colony col;
    ACOConfig acoConfig;

    public Ant(Position p, Colony c, ACOConfig acoConfig) {
        this.startingposition = p;
        this.col = c;
        this.acoConfig = acoConfig;
    }


    private void compareToBest() {
        Position pold = null;
        int fitness = 0;
        for (Position p : solution) {
            if (pold != null) {
                fitness += Conversion.getPesoDeslocarPosicaoEuclidiana(pold.getString(), pold.getFret(), p.getString(), p.getFret());
            }
            pold = p;
        }
        if (fitness < col.bestfitness) {
            col.bestsolution = solution;
            col.bestfitness = fitness;
        }
        if (fitness < col.bestant) {
            col.bestant = fitness;
        }
    }

    private Position move(Position p) throws InvalidMoveException {
        Position chosenposition = null;
        if (!p.getRoutes().isEmpty()) {
            Map<Position, Path> routes = p.getRoutes();
            List<Position> positions = new ArrayList<>();
            List<Double> probabilities = new ArrayList<>();
            double prob = 0, proball = 0, distance = 0;
            for (Position position : routes.keySet()) {
                if (routes.get(position).getDistance() > 0) {
                    prob = Math.pow(routes.get(position).getTrail(), acoConfig.getA()) * Math.pow((1 / routes.get(position).getDistance()), acoConfig.getB());
                } else {
                    prob = Math.pow(routes.get(position).getTrail(), acoConfig.getA()) * Math.pow((1 / 0.99), acoConfig.getB());
                }
                positions.add(position);
                probabilities.add(prob);
                proball += prob;
            }
            for (int i = 0; i < probabilities.size(); i++) {
                probabilities.set(i, (probabilities.get(i) / proball));
            }
            if (proball == 0) {
                chosenposition = chooseRandomNextPosition(positions);
            } else {
                chosenposition = chooseByProbability(positions, probabilities);
            }
            if (chosenposition == null) {
                throw new InvalidMoveException("Error while ant was moving, null next position");
            }
        } else {
            return null;
        }
        return chosenposition;
    }

    private Position chooseByProbability(List<Position> positions, List<Double> probabilities) {
        Random rand = new Random();
        double chosenprob = rand.nextFloat();
        double accumulatedprob = 0;
        for (int i = 0; i < positions.size(); i++) {
            accumulatedprob += probabilities.get(i);
            if (chosenprob <= accumulatedprob) {
                return positions.get(i);
            }
        }
        return null;
    }

    private Position chooseRandomNextPosition(List<Position> positions) {
        Random rand = new Random();
        return positions.get(rand.nextInt(positions.size()));
    }

    private void setTrailInSolution() {
        Position pold = null;
        for (Position p : solution) {
            if (pold != null) {
                pold.getPath(p).incrementAntspassing();
            }
            pold = p;
        }
    }

    @Override
    public List<Position> call() throws InvalidMoveException {
        Position chosenpos = this.startingposition;
        do {
            solution.add(chosenpos);
            chosenpos = move(chosenpos);
        } while (chosenpos != null);
        traillock.lock();
        setTrailInSolution();
        compareToBest();
        traillock.unlock();
        return solution;
    }
}
