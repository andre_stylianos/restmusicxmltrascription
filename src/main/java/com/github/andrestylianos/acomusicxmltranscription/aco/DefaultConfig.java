package com.github.andrestylianos.acomusicxmltranscription.aco;

public class DefaultConfig implements ACOConfig {

    private double b = 1;

    @Override
    public int getNumberOfIterations() {
        return 20000;
    }

    @Override
    public double getA() {
        return 0.15;
    }

    @Override
    public double getB() {
        return b;
    }

    @Override
    public void incrementB() {
        b *= 2;
    }

    @Override
    public double getQ() {
        return 200;
    }

    @Override
    public double getP() {
        return 0.5;
    }

    @Override
    public int getMaxStagnation() {
        return 500;
    }

    @Override
    public double getInitialTrail() {
        return 0.1;
    }
}
