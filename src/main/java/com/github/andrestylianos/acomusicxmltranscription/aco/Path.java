package com.github.andrestylianos.acomusicxmltranscription.aco;

public class Path {
    private double distance;
    private double trail;
    private int antspassing = 0;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getTrail() {
        return trail;
    }

    public void setTrail(double trail) {

        this.trail = trail;
    }

    public int getAntspassing() {
        return antspassing;
    }

    public void incrementAntspassing() {
        this.antspassing++;
    }

    public void resetAntspasssing() {
        this.antspassing = 0;
    }
}
