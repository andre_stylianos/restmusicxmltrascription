package com.github.andrestylianos.acomusicxmltranscription.aco;

import com.github.andrestylianos.acomusicxmltranscription.music.tunings.Tuning;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Graph {
    private List<List<Position>> notes = new ArrayList<>();
    private List<Position> startingpositions = new ArrayList<>();
    private Colony col;
    private ACOConfig acoConfig;

    public Graph(String[][] music, Colony c, Tuning t, ACOConfig acoConfig) {
        this.col = c;
        int freq = 0, id;
        this.acoConfig = acoConfig;
        List<Position> positions;
        for (String[] aMusic : music) {
//            freq = Conversion.getIdFreqNota(aMusic[0], aMusic[1]);
//            //            System.out.println("Frequencia: " + freq);
//            positions = new ArrayList<>();
//            try {
//                for (int y = 1; y < Conversion.matrizPosicoesNotasId[freq - 40].length - 2; y++) {
//                    id = Conversion.matrizPosicoesNotasId[freq - 40][y];
//                    if (id != -1) {
//                        //                    System.out.println(id);
//                        positions.add(new Position(Conversion.matrizIdPosicoes[id][1], Conversion.matrizIdPosicoes[id][2], acoConfig));
//                        //                    System.out.println("Corda: " + Conversion.matrizIdPosicoes[id][1] + "Casa: " + Conversion.matrizIdPosicoes[id][2]);
//                    }
//                }
//            } catch (IndexOutOfBoundsException e) {
//                System.err.println("Nota " + aMusic[0] + aMusic[1] + " não existe no braço do violão");
//            }
            positions = t.getPosForNote(aMusic[0], Integer.valueOf(aMusic[1])).stream().map(fretPos -> new Position(fretPos.getString(), fretPos.getFret(), acoConfig)).collect(Collectors.toList());
            notes.add(positions);
        }
    }

    public List<Position> getStartingPositions() {
        return startingpositions;
    }

    protected void constructGraph() {
        for (int x = 0; x < notes.size() - 1; x++) {
            for (Position p : notes.get(x)) {
                for (Position pnext : notes.get(x + 1)) {
                    p.addRoute(pnext);
                }
            }
        }
        startingpositions = notes.get(0);
    }

    public void updateGraphTrails() {
        for (List<Position> lp : notes) {
            for (Position p : lp) {
                updateTrail(p);
            }
        }
    }

    private void updateTrail(Position p) {
        double trail;
        Path path;
        for (Position pnext : p.getRoutes().keySet()) {
            path = p.getPath(pnext);
            trail = calculateNewTrail(path);
            path.setTrail(trail);
        }

    }

    private double calculateNewTrail(Path path) {
        double trail;
        double deltatrail = 0;
        double distance;
        if (path.getDistance() > 0) {
            distance = path.getDistance();
        } else {
            distance = 0.99;
        }
        deltatrail = path.getAntspassing() * (acoConfig.getQ() / distance);
        trail = ((1 - acoConfig.getP()) * path.getTrail()) + deltatrail;
        path.resetAntspasssing();
        return trail;
    }
}
