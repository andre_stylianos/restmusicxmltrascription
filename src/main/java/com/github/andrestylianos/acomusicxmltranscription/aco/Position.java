package com.github.andrestylianos.acomusicxmltranscription.aco;

import java.util.HashMap;
import java.util.Map;

public class Position {
    private Map<Position, Path> routes = new HashMap<>();
    private int string, fret;
    private ACOConfig acoConfig;

    public Position(int string, int fret, ACOConfig acoConfig) {
        this.string = string;
        this.fret = fret;
        this.acoConfig = acoConfig;
    }

    protected void addRoute(Position pnext) {
        Path path = new Path();
        path.setTrail(acoConfig.getInitialTrail());
        //Distancia utilizando a heuristica criada pelo João
//        path.setDistance(Conversion.getPesoDeslocarPosicao(string,fret,pnext.getString(),pnext.getFret()));
        //Distancia de manhattan
        if (pnext.getFret() > 0) {
//            path.setDistance(Math.abs(string - pnext.getString()) + Math.abs(fret - pnext.getFret()));
            path.setDistance(Math.sqrt(Math.pow(string - pnext.getString(), 2) + Math.pow(fret - pnext.getFret(), 2)));
        } else {
            path.setDistance(0);
        }
        routes.put(pnext, path);
    }

    protected Path getPath(Position pnext) {
        return routes.get(pnext);
    }

    public int getString() {
        return string;
    }

    public int getFret() {
        return fret;
    }

    public Map<Position, Path> getRoutes() {
        return routes;
    }

    @Override
    public String toString() {
        return "[" + "<" + string + "," + fret + ">" + "]";
    }
}

