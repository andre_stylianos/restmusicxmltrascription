package com.github.andrestylianos.acomusicxmltranscription.aco;

import com.github.andrestylianos.acomusicxmltranscription.music.MusicXML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Colony {

    private static final Logger logger = LoggerFactory.getLogger(Colony.class);

    protected List<Position> bestsolution;
    protected int bestfitness = Integer.MAX_VALUE, bestant;
    //    private List<Thread> ants;
    private int number_ants = 5;
    private ExecutorService executor = Executors.newFixedThreadPool(6);

    public MusicXML runColony(MusicXML music, ACOConfig acoConfig) {
        double start, tempo;
        Graph graph;
        Thread t;
        int bestfromiter, stagnation;
        start = System.currentTimeMillis();
        bestfitness = Integer.MAX_VALUE;
        graph = new Graph(music.getNotes(), this, music.getTuning(), acoConfig);
        graph.constructGraph();
        List<Position> startingpositions = graph.getStartingPositions();
        bestfromiter = bestfitness;
        stagnation = 0;
        List<Future<List<Position>>> futures;
        for (int x = 0; x < acoConfig.getNumberOfIterations(); x++) {
            futures = new ArrayList<>();
            bestant = Integer.MAX_VALUE;
            for (Position p : startingpositions) {
                futures.add(executor.submit(new Ant(p, this, acoConfig)));
            }
            try {
                for (Future<List<Position>> f : futures) {
                    f.get();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            graph.updateGraphTrails();
            if (bestfromiter == bestfitness) {
                stagnation++;
            } else {
                stagnation = 0;
            }
            bestfromiter = bestfitness;
            if (stagnation == 100)
                acoConfig.incrementB();
            if (stagnation == 400)
                acoConfig.incrementB();
            if (stagnation >= acoConfig.getMaxStagnation()) {
//              logger.debug("Best path found at iteration n {} by stagnation", x);
                break;
            }

        }
        executor.shutdown();
        try {
            executor.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.debug("Fitness: {}", bestfitness);
        // parser.writeXML(bestsolution);
        tempo = System.currentTimeMillis() - start;
        logger.debug("Time: {} sec", tempo / 1000);
//		System.out.println(y+";"+bestfitness+";"+tempo+";"+bestsolution.toString());
//		pw.write(y+";"+bestfitness+";"+tempo+";"+bestsolution.toString()+"\n");
//        return "Resultado pronto! \nFitness: "+bestfitness+"\nTime: "+ tempo/1000;
        music.addTabDetails(bestsolution);
        return music;
    }
}