package com.github.andrestylianos.acomusicxmltranscription.util;


import com.audiveris.proxymusic.Note;
import com.audiveris.proxymusic.ScorePartwise;
import com.audiveris.proxymusic.util.Marshalling;
import nu.xom.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class XMLParser {

    public static String[][] parseXML(ScorePartwise sp) {
        String[][] music = null;
        List<Note> notes = new ArrayList<>();
        ScorePartwise.Part p = sp.getPart().get(0);
        for (ScorePartwise.Part.Measure m : p.getMeasure()) {
            for (Object o : m.getNoteOrBackupOrForward()) {
                if (o instanceof Note) {
                    Note n = (Note) o;
                    if (n.getPitch() != null)
                        notes.add(n);
                }
            }
        }


        music = new String[notes.size()][2];
        for (int x = 0; x < notes.size(); x++) {
            music[x][0] = notes.get(x).getPitch().getStep().name();
            if (notes.get(x).getPitch().getAlter() != null) {
                addAlter(music, notes, x);
            }
            music[x][1] = notes.get(x).getPitch().getOctave() + "";
        }
        return music;
    }

    private static void addAlter(String[][] music, List<Note> notes, int x) {
        switch (notes.get(x).getPitch().getAlter().toString()) {
            case "1":
                music[x][0] = Conversion.converteParaSustenido(music[x][0]);
                break;
            case "-1":
                music[x][0] = Conversion.converteParaBemol(music[x][0]);
                break;
            case "2":
                music[x][0] = Conversion.converteParaDuploSustenido(music[x][0]);
                break;
            case "-2":
                music[x][0] = Conversion.converteParaDuploBemol(music[x][0]);
        }
    }

    public static String[][] parseXMLFromString(String xml) throws JAXBException, FileNotFoundException, Marshalling.UnmarshallingException {
        String[][] music = null;
        List<Note> notes = new ArrayList<>();
        Marshalling.getContext(ScorePartwise.class);
        ScorePartwise sp = (ScorePartwise) Marshalling.unmarshal(new ByteArrayInputStream(xml.getBytes()));
        ScorePartwise.Part p = sp.getPart().get(0);
        for (ScorePartwise.Part.Measure m : p.getMeasure()) {
            for (Object o : m.getNoteOrBackupOrForward()) {
                if (o instanceof Note) {
                    Note n = (Note) o;
                    if (n.getPitch() != null)
                        notes.add(n);
                }
            }
        }


        music = new String[notes.size()][2];
//            System.out.println("JAXB"+notes.size());
        for (int x = 0; x < notes.size(); x++) {
            music[x][0] = notes.get(x).getPitch().getStep().name();
            if (notes.get(x).getPitch().getAlter() != null) {
                addAlter(music, notes, x);
            }
            music[x][1] = notes.get(x).getPitch().getOctave() + "";
        }
        return music;
    }

    public static List<Note> getNotesFromXML(File xml) {
        String[][] music = null;
        List<Note> notes = new ArrayList<>();
        try {
            Marshalling.getContext(ScorePartwise.class);
            ScorePartwise sp = (ScorePartwise) Marshalling.unmarshal(new FileInputStream(xml));
            ScorePartwise.Part p = sp.getPart().get(0);
            for (ScorePartwise.Part.Measure m : p.getMeasure()) {
                for (Object o : m.getNoteOrBackupOrForward()) {
                    if (o instanceof Note) {
                        Note n = (Note) o;
                        if (n.getPitch() != null)
                            notes.add(n);
                    }
                }
            }
        } catch (JAXBException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Marshalling.UnmarshallingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return notes;
    }

    public static String[][] parseXMLXOM(String path) {
        String[][] music = null;
        Document xml;
        try {
            Element part = null;
            XMLReader xmlreader = XMLReaderFactory.createXMLReader();
            xmlreader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            Builder builder = new Builder(xmlreader);
            xml = builder.build(new File(path));
            Element root = xml.getRootElement();
            if (root.getChildElements("part").size() == 1) {
                part = root.getChildElements("part").get(0);
            } else if (root.getChildElements("part").size() > 1) {
                for (int i = 0; i < root.getChildElements("part").size(); i++) {
                    String partid = root.getChildElements("part").get(i).getAttributeValue("id");
                    if (partid.toLowerCase().equals("piano") || partid.toLowerCase().equals("p1")) {
                        part = root.getChildElements("part").get(i);
                    }
                }
            } else {
                System.err.println("Não existe elemento part. ERRO.");
                System.exit(0);
            }
            Nodes notes = part != null ? part.query("measure/note/pitch") : null;
//            System.out.println(notes.size());
            music = new String[notes.size()][2];
            Element alter;
            String step, octave;
//            System.out.println("XOM"+notes.size());
            for (int i = 0; i < notes.size(); i++) {
                Element note = (Element) notes.get(i);
                step = note.getFirstChildElement("step").getValue();
                octave = note.getFirstChildElement("octave").getValue();
                alter = note.getFirstChildElement("alter");
                if (alter != null) {
                    switch (alter.getValue()) {
                        case "1":
                            step = Conversion.converteParaSustenido(step);
                            break;
                        case "-1":
                            step = Conversion.converteParaBemol(step);
                            break;
                        case "2":
                            step = Conversion.converteParaDuploSustenido(step);
                            break;
                        case "-2":
                            step = Conversion.converteParaDuploBemol(step);
                    }
                }
                music[i][0] = step;
                music[i][1] = octave;
            }
            //System.exit(0);
        } catch (ParsingException ex) {
            System.err.println("Arquivo mal formado.");
//            System.exit(0);
        } catch (IOException ex) {
            System.err.println("Arquivo não existe.");
            System.exit(0);
        } catch (SAXNotSupportedException e) {
            e.printStackTrace();
        } catch (SAXNotRecognizedException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return music;
    }
}
