package com.github.andrestylianos.acomusicxmltranscription.util;

import com.audiveris.proxymusic.*;
import com.audiveris.proxymusic.String;
import com.github.andrestylianos.acomusicxmltranscription.aco.Position;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class XMLCreator {
    ScorePartwise sp;

    public XMLCreator(ScorePartwise sp) {
        this.sp = sp;
    }

    public void addTabDetails(List<Position> neckpositions) {
        addStaffDetails();
        addTabs(neckpositions);
    }

    public void addTabs(List<Position> neckpositions) {

        List<Note> notes = new ArrayList<>();
        ScorePartwise.Part p = sp.getPart().get(0);
        for (ScorePartwise.Part.Measure m : p.getMeasure()) {
            for (Object o : m.getNoteOrBackupOrForward()) {
                if (o instanceof Note) {
                    Note n = (Note) o;
                    if (n.getPitch() != null)
                        notes.add(n);
                }
            }
        }
        for (int i = 0; i < notes.size(); i++) {
            Position pos = neckpositions.get(i);
            ObjectFactory of = new ObjectFactory();
            Notations not = of.createNotations();
            Technical tec = of.createTechnical();
            Fret fret = of.createFret();
            String string = of.createString();
            fret.setValue(BigInteger.valueOf(pos.getFret()));
            string.setValue(BigInteger.valueOf(pos.getString()));
            tec.getUpBowOrDownBowOrHarmonic().add(of.createTechnicalFret(fret));
            tec.getUpBowOrDownBowOrHarmonic().add(of.createTechnicalString(string));
            not.getTiedOrSlurOrTuplet().add(tec);
            notes.get(i).getNotations().add(not);

        }

    }

    public void addStaffDetails() {

        ObjectFactory of = new ObjectFactory();
        ScorePartwise.Part.Measure m = sp.getPart().get(0).getMeasure().get(0);
        Attributes att = null;
        for (Object obj : m.getNoteOrBackupOrForward()) {
            if (obj instanceof Attributes) {
                att = (Attributes) obj;
                break;
            }
        }
        if (att == null) {
            att = of.createAttributes();
            m.getNoteOrBackupOrForward().add(0, att);
        }
//        boolean clefexists = false;
//        for(Clef clef : att.getClef()){
//            if(clef.getSign().equals(ClefSign.TAB)) clefexists =true;
//        }
        boolean clefexists = att.getClef().stream().anyMatch(clef -> clef.getSign().equals(ClefSign.TAB));
        if (!clefexists) {
            Clef clef = of.createClef();
            clef.setNumber(BigInteger.valueOf(2));
            clef.setSign(ClefSign.TAB);
            clef.setLine(BigInteger.valueOf(5));
            att.getClef().add(clef);
        }
        att.setStaves(BigInteger.valueOf(2));

//        if(att.getStaffDetails().parallelStream().anyMatch(sd -> sd.getStaffLines()==null)){
        StaffDetails sd = of.createStaffDetails();
        sd.setNumber(BigInteger.valueOf(2));
        sd.setStaffLines(BigInteger.valueOf(6));
        List<StaffTuning> st = sd.getStaffTuning();
        StaffTuning tuning = of.createStaffTuning();
        tuning.setLine(BigInteger.ONE);
        tuning.setTuningStep(Step.E);
        tuning.setTuningOctave(2);
        st.add(tuning);
        tuning = of.createStaffTuning();
        tuning.setLine(BigInteger.valueOf(2));
        tuning.setTuningStep(Step.A);
        tuning.setTuningOctave(2);
        st.add(tuning);
        tuning = of.createStaffTuning();
        tuning.setLine(BigInteger.valueOf(3));
        tuning.setTuningStep(Step.D);
        tuning.setTuningOctave(3);
        st.add(tuning);
        tuning = of.createStaffTuning();
        tuning.setLine(BigInteger.valueOf(4));
        tuning.setTuningStep(Step.G);
        tuning.setTuningOctave(3);
        st.add(tuning);
        tuning = of.createStaffTuning();
        tuning.setLine(BigInteger.valueOf(5));
        tuning.setTuningStep(Step.B);
        tuning.setTuningOctave(3);
        st.add(tuning);
        tuning = of.createStaffTuning();
        tuning.setLine(BigInteger.valueOf(6));
        tuning.setTuningStep(Step.E);
        tuning.setTuningOctave(4);
        st.add(tuning);
        att.getStaffDetails().add(sd);
//        }
    }
}
