package com.github.andrestylianos.acomusicxmltranscription.util;

import com.audiveris.proxymusic.ScorePartwise;
import com.audiveris.proxymusic.util.Marshalling;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class XMLHandler {

    ScorePartwise sp;

    public XMLHandler(File xml) throws JAXBException, FileNotFoundException, Marshalling.UnmarshallingException {
        Marshalling.getContext(ScorePartwise.class);
        ScorePartwise sp = (ScorePartwise) Marshalling.unmarshal(new FileInputStream(xml));
    }

    public String[][] parseXML() {
        return null;
    }
}
