package com.github.andrestylianos.acomusicxmltranscription.music.tunings;

import java.util.List;

public class Tuning {

    private String tuningid;
    private List<Note> notes;

    public String getTuningID() {
        return tuningid;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public List<FretPos> getPosForNote(String note, int octave) {
        for (Note n : notes) {
            if (n.getNote().equals(note) && n.getOctave() == octave) {
                return n.getPosition();
            }
        }
        return null;
    }
}
