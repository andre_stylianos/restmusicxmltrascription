package com.github.andrestylianos.acomusicxmltranscription.music.tunings;

/**
 * Created by pantera on 16/10/15.
 */
public class TuningNotFoundException extends Exception {
    public TuningNotFoundException(){ super();}

    public TuningNotFoundException(String message){ super(message);}
}
