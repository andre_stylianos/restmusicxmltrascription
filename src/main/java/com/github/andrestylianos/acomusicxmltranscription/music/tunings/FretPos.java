package com.github.andrestylianos.acomusicxmltranscription.music.tunings;

public class FretPos {
    private int string;
    private int fret;

    protected FretPos() {

    }

    public int getString() {
        return string;
    }

    public int getFret() {
        return fret;
    }
}
