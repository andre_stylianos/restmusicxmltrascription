package com.github.andrestylianos.acomusicxmltranscription.music;

import com.audiveris.proxymusic.ScorePartwise;
import com.audiveris.proxymusic.util.Marshalling;
import com.github.andrestylianos.acomusicxmltranscription.aco.Position;
import com.github.andrestylianos.acomusicxmltranscription.music.tunings.Tuning;
import com.github.andrestylianos.acomusicxmltranscription.util.XMLCreator;
import com.github.andrestylianos.acomusicxmltranscription.util.XMLParser;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.List;

public class MusicXML {

    private ScorePartwise sp;
    private String[][] notes;
    private Tuning t;

    public MusicXML(Path path) throws JAXBException, FileNotFoundException, Marshalling.UnmarshallingException {
        Marshalling.getContext(ScorePartwise.class);
        sp = (ScorePartwise) Marshalling.unmarshal(new FileInputStream(new File(path.toString())));
    }

    public MusicXML(ScorePartwise sp) {
        this.sp = sp;
    }

    public MusicXML(String xml, Tuning t) throws JAXBException, Marshalling.UnmarshallingException {
        Marshalling.getContext(ScorePartwise.class);
        sp = (ScorePartwise) Marshalling.unmarshal(new ByteArrayInputStream(xml.getBytes()));
        this.t = t;
    }

    public Tuning getTuning() {
        return t;
    }

    public void setTuning(Tuning t) {
        this.t = t;
    }

    public String[][] getNotes() {
        if (notes == null) {
            notes = XMLParser.parseXML(sp);
        }
        return notes;
    }

    public void addTabDetails(List<Position> positions) {
        XMLCreator creator = new XMLCreator(sp);
        creator.addTabDetails(positions);
    }

    public ScorePartwise getTabbedMusicXML() {
        return sp;
    }


}
