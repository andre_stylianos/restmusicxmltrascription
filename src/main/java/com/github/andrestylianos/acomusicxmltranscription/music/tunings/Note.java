package com.github.andrestylianos.acomusicxmltranscription.music.tunings;

import java.util.List;

public class Note {

    private String note;
    private int octave;
    private List<FretPos> position;

    protected Note() {

    }

    public List<FretPos> getPosition() {
        return position;
    }

    public String getNote() {

        return note;
    }

    public int getOctave() {
        return octave;
    }

    public String toString() {
        return "<" + note + "," + octave + ">\n";
    }
}
