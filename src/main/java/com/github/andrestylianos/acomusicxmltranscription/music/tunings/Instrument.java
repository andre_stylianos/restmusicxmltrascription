package com.github.andrestylianos.acomusicxmltranscription.music.tunings;

import com.google.gson.Gson;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class Instrument {

    private List<Tuning> tunings;
    private String instrument;

    private Instrument() {

    }

    public static Instrument getInstrument(String instrument) throws IOException {
        Instrument i = null;
        if(instrument.equals("violao")) {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStreamReader in = new InputStreamReader(classLoader.getResourceAsStream("/tunings/violao.json"));
            BufferedReader br = new BufferedReader(in);
            Gson gson = new Gson();
            i = gson.fromJson(br, Instrument.class);
        }
        return i;
    }

    public Tuning getTuning(String tuning) throws TuningNotFoundException {
        for (Tuning t : tunings) {
            if (t.getTuningID().equalsIgnoreCase(tuning)) return t;
        }
        throw new TuningNotFoundException();
    }
}
