package com.github.andrestylianos.acomusicxmltranscription.restapi;

import com.audiveris.proxymusic.ScorePartwise;
import com.audiveris.proxymusic.util.Marshalling;
import com.github.andrestylianos.acomusicxmltranscription.aco.Colony;
import com.github.andrestylianos.acomusicxmltranscription.aco.DefaultConfig;
import com.github.andrestylianos.acomusicxmltranscription.music.MusicXML;
import com.github.andrestylianos.acomusicxmltranscription.music.tunings.Instrument;
import com.github.andrestylianos.acomusicxmltranscription.music.tunings.Tuning;
import com.github.andrestylianos.acomusicxmltranscription.music.tunings.TuningNotFoundException;
import com.github.andrestylianos.acomusicxmltranscription.restapi.beans.MusicBean;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@Path("transcript")
public class TranscriptResource {

    @POST
    @Produces("application/vnd.recordare.musicxml+xml")
    @Consumes("application/vnd.recordare.musicxml+xml")
    public ScorePartwise getResult(ScorePartwise xml) {
        Colony col = new Colony();
        MusicXML tabbedxml = col.runColony(new MusicXML(xml), new DefaultConfig());
        return tabbedxml.getTabbedMusicXML();
    }

    @POST
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getResult(MusicBean music) {
        Colony col = new Colony();
        try {
            Tuning t = Instrument.getInstrument(music.getInstrument()).getTuning(music.getTuning());
            MusicXML tabbedxml = col.runColony(new MusicXML(music.getXml(), t), new DefaultConfig());
//            return tabbedxml.getTabbedMusicXML();
            return Response.ok(tabbedxml.getTabbedMusicXML(), "application/vnd.recordare.musicxml+xml").header("Content-Disposition", "attachment;filename=aaaaaaa.xml").build();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (Marshalling.UnmarshallingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            return Response.serverError().entity("Instrument not found...").build();
        } catch (TuningNotFoundException e) {
            return Response.serverError().entity("Tuning not found...").build();
        }
        return null;
    }

}
