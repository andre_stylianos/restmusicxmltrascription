package com.github.andrestylianos.acomusicxmltranscription.restapi.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MusicBean {

    public MusicBean(){
    }

    public String tuning;

    public String instrument;

    public String xml;


    public String getTuning() {
        return tuning;
    }

    public String getInstrument() {
        return instrument;
    }

    public String getXml() {
        return xml;
    }
}
