package com.github.andrestylianos.acomusicxmltranscription.restapi.providers;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

public class TranscriptResourceExceptions {

    public static WebApplicationException createWebAppException(Response.Status status, Map<String, String> jsonproperties) {
        JsonObject json = new JsonObject();
        for (String key : jsonproperties.keySet()) {
            json.addProperty(key, jsonproperties.get(key));
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
        return new WebApplicationException(Response.status(status).entity(gson.toJson(json)).type(MediaType.APPLICATION_JSON_TYPE).build());
    }
}
