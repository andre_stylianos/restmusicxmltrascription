package com.github.andrestylianos.acomusicxmltranscription.restapi.providers;

import com.audiveris.proxymusic.ScorePartwise;
import com.audiveris.proxymusic.util.Marshalling;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;


@Provider
@Consumes("application/vnd.recordare.musicxml+xml")
@Produces("application/vnd.recordare.musicxml+xml")
public class MusicXMLProvider implements MessageBodyReader<ScorePartwise>, MessageBodyWriter<ScorePartwise> {


    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == ScorePartwise.class;
    }

    @Override
    public ScorePartwise readFrom(Class<ScorePartwise> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException, WebApplicationException {
        try {
            Marshalling.getContext(ScorePartwise.class);
            return (ScorePartwise) Marshalling.unmarshal(entityStream);
        } catch (JAXBException e) {
            Map<String, String> jsonproperties = new HashMap<>();
            jsonproperties.put("error", "Error deserealizing a ScorePartwise");
            throw TranscriptResourceExceptions.createWebAppException(Response.Status.INTERNAL_SERVER_ERROR, jsonproperties);
        } catch (Marshalling.UnmarshallingException e) {
            Map<String, String> jsonproperties = new HashMap<>();
            jsonproperties.put("error", "Error unmarshalling a ScorePartwise. Not a valid MusicXML file?");
            throw TranscriptResourceExceptions.createWebAppException(Response.Status.INTERNAL_SERVER_ERROR, jsonproperties);
        }
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == ScorePartwise.class;
    }

    @Override
    public long getSize(ScorePartwise scorePartwise, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return 1;
    }

    @Override
    public void writeTo(ScorePartwise scorePartwise, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        try {
            Marshalling.marshal(scorePartwise, entityStream, false, 2);
        } catch (Marshalling.MarshallingException e) {
            Map<String, String> jsonproperties = new HashMap<>();
            jsonproperties.put("error", "Marshalling exception, internal server error");
            throw TranscriptResourceExceptions.createWebAppException(Response.Status.INTERNAL_SERVER_ERROR, jsonproperties);
        }
    }
}
