#### This code was developed as part of a research project and its extension into a web service was done as part of my undergraduate thesis. There are many areas that can be improved should this be used for further research. This was developed in a learning environment, unfortunately results were the goal and coding best practices were left a little on the sidelines.

### What is this repository for? ###

* REST service for the transcription of partitures into tabs.

### Accepted file type ###

* [MusicXML](http://www.musicxml.com/)
* Songs used during development from [The Session](www.thesession.org)

### How do I get set up? ###

* [Gradle](www.gradle.org) build tool
* Dependency [Proxymusic](https://proxymusic.kenai.com/) must be downloaded manually into folder `lib/`
* Tests are still being written
* Uses gradle tasks for building and running
     * `./gradlew build`
     * `./gradlew jettyRun`
     * `./gradlew jettyStop`

### Contribution guidelines ###

* Writing tests
* Code review